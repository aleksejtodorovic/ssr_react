const path = require("path");
const webpackMerge = require("webpack-merge");
const baseConf = require('./webpack.base');

const conf = {
  entry: "./src/client/client.js",

  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "public")
  }
};

module.exports = webpackMerge(baseConf, conf);
