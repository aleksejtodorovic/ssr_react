import React from "react";
import { renderToString } from "react-dom/server";
import { StaticRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { renderRoutes } from "react-router-config";
import serialize from 'serialize-javascript';

import Routes from "../client/Routes";
import Header from "../client/components/Header";

export default ({ path }, store) => {
  const content = renderToString(
    <Provider store={store}>
      <StaticRouter context={{}} location={path}>
        <div>
          <Header />
          <div>{renderRoutes(Routes)}</div>
        </div>
      </StaticRouter>
    </Provider>
  );

  return `
    <html>
      <head>
      </head>
      <body>
        <div id="root">${content}</div>
        <script>
          window.INITIAL_STORE = ${serialize(store.getState())}
        </script>
        <script src="bundle.js"> </script>
      </body>
    </html>
  `;
};
