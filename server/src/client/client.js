import "babel-polyfill";
import React from "react";
import { hydrate } from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { Provider } from "react-redux";
import { renderRoutes } from "react-router-config";

import Routes from "./Routes";
import reducers from "./reducers";
import Header from './components/Header';

const store = createStore(reducers, window.INITIAL_STORE || {}, applyMiddleware(thunk));

hydrate(
  <Provider store={store}>
    <BrowserRouter>
      <div>
        <Header />
        <div>{renderRoutes(Routes)}</div>
      </div>
    </BrowserRouter>
  </Provider>,
  document.querySelector("#root")
);
