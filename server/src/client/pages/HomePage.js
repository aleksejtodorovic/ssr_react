import React from "react";

class Home extends React.Component {
  render() {
    return (
      <div>
        <div>This is homepage test.</div>
        <button onClick={() => console.log("a click occured")}>Click me</button>
      </div>
    );
  }
}

export default {
  component: Home
};
