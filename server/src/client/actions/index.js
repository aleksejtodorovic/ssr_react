import axios from 'axios';

export const FETCH_USERS = 'FETCH_USERS';

const api = axios.create({
    baseURL: 'https://react-ssr-api.herokuapp.com'
});

export const fetchUsers = () => async dispatch => {
    const res = await api.get('/users');

    dispatch({
        type: FETCH_USERS,
        payload: res
    });
}