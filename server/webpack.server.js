const path = require("path");
const webpackMerge = require("webpack-merge");
const baseConf = require('./webpack.base');
const webpackNodeExternals = require('webpack-node-externals');

const conf = {
  target: "node",

  entry: "./src/index.js",

  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "build")
  },

  externals: [webpackNodeExternals()]
};

module.exports = webpackMerge(baseConf, conf)
